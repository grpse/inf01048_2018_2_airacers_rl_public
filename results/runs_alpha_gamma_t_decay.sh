# LEONARDO RODA ESSES
# ALPHA TEST GAMMA FIXED T FIXED T DECAY FIXED REWARD TYPE FIXED
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.1 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.9 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \

# GAMMA TEST ALPHA FIXED T FIXED T DECAY FIXED REWARD TYPE FIXED
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.20 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.80 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \

# T TEST ALPHA FIXED GAMMA FIEX
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 100000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \


# GILBERTO RODA ESSES
(/usr/bin/time -f "T 10000 DECAY 0.01 %E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.999 --terperature 10000 --decay 0.01 --reward good_features --discretization interval_discretization learn) 2>> times.txt & \
(/usr/bin/time -f "T 100000 DECAY 0.01 %E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.999 --terperature 100000 --decay 0.01 --reward good_features --discretization interval_discretization learn) 2>> times.txt & \

#running
# REWARDS TEST ALPHA FIXED GAMMA FIXED T FIXED T DECAY FIXED
(/usr/bin/time -f "R few_features %E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.01 --reward few_features --discretization interval_discretization learn) 2>> times.txt & \
(/usr/bin/time -f "R good_features %E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.01 --reward good_features --discretization interval_discretization learn) 2>> times.txt & \
(/usr/bin/time -f "R all_features %E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.01 --reward all_features --discretization interval_discretization learn) 2>> times.txt & \

# DISCRETIZATIONS TEST ALL OTHER PARAMETERS FIXED
(/usr/bin/time -f "%D integer E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward all_features --discretization integer_discretization learn) 2>> times.txt & \
(/usr/bin/time -f "%D interval E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward all_features --discretization interval_discretization learn) 2>> times.txt

# GULOUS STRATEGY
(/usr/bin/time -f "%D interval E real,%U user,%S sys /n" python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 0 --decay 0.1 --reward all_features --discretization interval_discretization learn) 2>> times.txt
