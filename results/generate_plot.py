import matplotlib.pyplot as plt
import sys
import csv

input_file = './results.csv'

episode = []

alpha_0_1 = []
alpha_0_5 = []
alpha_0_9 = []

gamma_0_2  = []
gamma_0_8  = []
gamma_0_99 = []

T_0           = []
T_10000_D_1   = []
T_10000_D_10  = []
T_100000_D_1  = []
T_100000_D_10 = []

few_features  = []
good_features = []
all_features  = []

discretization_integer  = []
discretization_interval = []

best_results = []

with open(input_file, 'r') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=',')
    i = 0
    for row in csv_reader:
        if i >= 6:
            episode.append(int(row[0]))

            alpha_0_1.append(float(row[1]))
            alpha_0_5.append(float(row[2]))
            alpha_0_9.append(float(row[3]))

            gamma_0_2.append(float(row[4]))
            gamma_0_8.append(float(row[5]))
            gamma_0_99.append(float(row[6]))

            T_0.append(float(row[7]))
            T_10000_D_1.append(float(row[8]))
            T_10000_D_10.append(float(row[9]))
            T_100000_D_1.append(float(row[10]))
            T_100000_D_10.append(float(row[11]))
            
            few_features.append(float(row[12]))
            good_features.append(float(row[13]))
            all_features.append(float(row[14]))
            
            discretization_integer.append(float(row[15]))
            discretization_interval.append(float(row[16]))
            
            best_results.append(float(row[17]))
        i = i + 1

plt.figure(1)
plt.suptitle('Alpha', y=0.97, fontsize=14)
plt.title('Gamma = 0.99; Temp = 10000; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, alpha_0_1, '-', label='0.1')
plt.plot(episode, alpha_0_5, '-', label='0.5')
plt.plot(episode, alpha_0_9, '-', label='0.9')
plt.legend(loc='upper left')
plt.savefig('./figures/1_alpha.png')

plt.figure(2)
plt.suptitle('Gamma', y=0.97, fontsize=14)
plt.title('Alpha = 0.5; Temp = 10000; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, gamma_0_2, '-', label='0.2')
plt.plot(episode, gamma_0_8, '-', label='0.8')
plt.plot(episode, gamma_0_99, '-', label='0.99')
plt.legend(loc='upper left')
plt.savefig('./figures/2_gamma.png')

plt.figure(3)
plt.suptitle('Exploration - Temperature', y=0.97, fontsize=14)
plt.title('Alpha = 0.5; Gamma = 0.99; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, T_0, '-', label='T = 0')
plt.plot(episode, T_10000_D_10, '-', label='T = 10000')
plt.plot(episode, T_100000_D_10, '-', label='T = 100000')
plt.legend(loc='upper left')
plt.savefig('./figures/3_temp.png')

plt.figure(4)
plt.suptitle('Exploration - Decay', y=0.97, fontsize=14)
plt.title('Alpha = 0.5; Gamma = 0.99; Temp = 100000*', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, T_0, '-', label='T = 0')
plt.plot(episode, T_100000_D_1, '-', label='D = 1%')
plt.plot(episode, T_100000_D_10, '-', label='D = 10%')
plt.legend(loc='upper left')
plt.savefig('./figures/4_decay.png')

plt.figure(5)
plt.suptitle('Reward', y=0.97, fontsize=14)
plt.title('Alpha = 0.5; Gamma = 0.99; Temp = 10000; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, few_features, '-', label='Few Features')
plt.plot(episode, good_features, '-', label='\"Good\" Features')
plt.plot(episode, all_features, '-', label='All Features')
plt.legend(loc='upper left')
plt.savefig('./figures/5_reward.png')

plt.figure(6)
plt.suptitle('Discretization', y=0.97, fontsize=14)
plt.title('Alpha = 0.5; Gamma = 0.99; Temp = 10000; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, discretization_integer, '-', label='Discretization Integer')
plt.plot(episode, discretization_interval, '-', label='Discretization Interval')
plt.legend(loc='upper left')
plt.savefig('./figures/6_discretization.png')


plt.figure(7)
plt.suptitle('Best params', y=0.97, fontsize=14)
plt.title('Alpha = 0.1; Gamma = 0.2; Temp = 100000; Decay = 10%', fontsize=10)
plt.ylabel('Score')
plt.xlabel('Episode')
plt.plot(episode, best_results, '-', label='Best params')
plt.legend(loc='upper left')
plt.savefig('./figures/7_best_params.png')

