# LEONARDO RODA ESSES
# ALPHA TEST GAMMA FIXED T FIXED T DECAY FIXED REWARD TYPE FIXED
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.1 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.9 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \

# GAMMA TEST ALPHA FIXED T FIXED T DECAY FIXED REWARD TYPE FIXED
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.20 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.80 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
# duplicated python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \

# T TEST ALPHA FIXED GAMMA FIEX
# duplicated python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 100000 --decay 0.1 --reward good_features --discretization interval_discretization learn & \

python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 10000 --decay 0.01 --reward good_features --discretization interval_discretization learn & \
python3 AIRacers.py -e 1000 -t interlagos -b safe_bot --alpha 0.5 --gamma 0.99 --terperature 100000 --decay 0.01 --reward good_features --discretization interval_discretization learn & \
