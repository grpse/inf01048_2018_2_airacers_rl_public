import interfaces as controller_template
from itertools import product
from typing import Tuple, List
import csv
import sys
import numpy
import math
import random
from enum import IntEnum
import collections



f = open('out.txt', 'w')

def print_out(to_write):
    f.seek(0)
    f.write(to_write)

def clamp(value: float, vmin: float, vmax: float) -> float:
    return max(min(value, vmax), vmin)

class OrderedClassMembers(type):
    @classmethod
    def __prepare__(self, name, bases):
        return collections.OrderedDict()
        
    def __new__(self, name, bases, classdict):
        classdict['__ordered__'] = [ key for key in classdict.keys() if key not in ('__module__', '__qualname__') ]
        return type.__new__(self, name, bases, classdict)

class TrackType:
    ON_ICE = 2
    ON_TRACK = 1
    ON_GRASS = 0

class DiscretizationLevels(metaclass=OrderedClassMembers):
    INSIDE_STREET_MARGIN = 2
    DIST_LEFT = 5
    DIST_RIGHT = 5
    DIST_MIDDLE = 5
    DIST_NEXT_CHECKPOINT = 8
    CHECKPOINT = 2
    CAR_VELOCITY = 4
    DIST_DETECTED_ENEMY = 5
    INCOMING_TRACK = 3
    DIST_DETECTED_BOMB = 5
    ENEMY_ABS_ANGLE = 9
    BOMB_ABS_ANGLE = 9

    @staticmethod
    def values() -> Tuple:
        return [ getattr(DiscretizationLevels, attr) for attr in DiscretizationLevels.__ordered__ if not callable(getattr(DiscretizationLevels, attr)) and not attr.startswith("__")]

class State(controller_template.State):
    def __init__(self, sensors: list, discretization_type='integer_discretization'):
        self.sensors = sensors
        self.track_distance_left = sensors[0] / 100.0 # normalized
        self.track_distance_center = sensors[1] / 100.0 # normalized
        self.track_distance_right = sensors[2] / 100.0 # normalized
        self.on_track = sensors[3] # ternary {0, 1, 2}
        self.checkpoint_distance = sensors[4] #normalized
        self.car_velocity = (sensors[5] - 10.0)/190.0 # normalized
        self.enemy_distance = sensors[6] # normalized
        self.enemy_position_angle = sensors[7] # normalized
        self.enemy_detected = sensors[8] # normalized
        self.checkpoint = sensors[9] # binary
        self.incoming_track = sensors[10] # ternary {0, 1, 2}
        self.bomb_distance = sensors[12] # normalized probably more then 1
        self.bomb_position_angle = sensors[13] # normalized
        self.bomb_detected = sensors[11] #binary
        self.discretization_type = discretization_type

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the car (see below) and compute useful features for selecting an action
        The car has the following sensors:

        self.sensors contains (in order):
            0 track_distance_left: 1-100
            1 track_distance_center: 1-100
            2 track_distance_right: 1-100
            3 on_track: 0 if off track, 1 if on normal track, 2 if on ice
            4 checkpoint_distance: 0-???
            5 car_velocity: 10-200
            6 enemy_distance: -1 or 0-???
            7 enemy_position_angle: -180 to 180
            8 enemy_detected: 0 or 1.seek(0)
            9 checkpoint: 0 or 1
           10 incoming_track: 1 if normal track, 2 if ice track or 0 if car is off track
           11 bomb_distance = -1 or 0-??? isso tá invertido é 12, 13 e 11 a ordem correta
           12 bomb_position_angle = -180 to 180 
           13 bomb_detected = 0 or 1
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """

        # real features
        inside_street_margin = (1 if self.on_track == TrackType.ON_TRACK or self.on_track == TrackType.ON_ICE else -1)
        dist_left = self.track_distance_left * inside_street_margin
        dist_right = self.track_distance_right * inside_street_margin
        dist_middle = self.track_distance_center * inside_street_margin
        dist_detected_enemy = self.enemy_distance * self.enemy_detected # only care about distance when it's detected
        dist_detected_bomb = self.bomb_distance * self.bomb_detected # only care about distance when it's detected
        enemy_abs_angle = clamp(abs(self.enemy_position_angle), 0, 45)
        bomb_abs_angle = clamp(abs(self.bomb_position_angle), 0, 45)

        #OLD FEATURES
        return [
            inside_street_margin,
            dist_left,
            dist_right,
            dist_middle,
            self.checkpoint_distance,
            self.checkpoint,
            self.car_velocity,
            dist_detected_enemy,
            self.incoming_track,
            dist_detected_bomb,
            enemy_abs_angle,
            bomb_abs_angle
        ]

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        discretization_function = getattr(self, self.discretization_type)

        inside_street_margin = features[0]
        dist_left = discretization_function(0, 1, features[1], DiscretizationLevels.DIST_LEFT)
        dist_right = discretization_function(0, 1, features[2], DiscretizationLevels.DIST_RIGHT)
        dist_middle = discretization_function(0, 1, features[3], DiscretizationLevels.DIST_MIDDLE)
        checkpoint_distance = discretization_function(0, 150, features[4], DiscretizationLevels.DIST_NEXT_CHECKPOINT)
        checkpoint = features[5]
        car_velocity = discretization_function(-1, 1, features[6], DiscretizationLevels.CAR_VELOCITY)
        dist_detected_enemy = discretization_function(0, 100, features[7], DiscretizationLevels.DIST_DETECTED_ENEMY)
        incoming_track = features[8]
        dist_detected_bomb = discretization_function(0, 100, features[9], DiscretizationLevels.DIST_DETECTED_BOMB)
        enemy_abs_angle = discretization_function(0, 45, features[10], DiscretizationLevels.ENEMY_ABS_ANGLE)
        bomb_abs_angle = discretization_function(0, 45, features[11], DiscretizationLevels.BOMB_ABS_ANGLE)

        discrete_values = [
            inside_street_margin,
            dist_left,
            dist_right,
            dist_middle,
            checkpoint_distance,
            checkpoint,
            car_velocity,
            dist_detected_enemy,
            incoming_track,
            dist_detected_bomb,
            enemy_abs_angle,
            bomb_abs_angle
        ]

        return discrete_values


    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return DiscretizationLevels.values()

    @staticmethod
    def discretization_levels_string() -> str:
        s = ''
        for d in State.discretization_levels():
            s += '_' + str(d)
        return s

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]
    
    def get_discretized_features(self) -> list:
        return self.discretize_features(self.compute_features())

    def integer_discretization(self, vmin: float, vmax: float, value: float, levels: int) -> int:
        step = distance = round(vmax - vmin)
        max_discret = round(vmax * (levels - 1))
        min_discret = round(vmin * levels)
        min_inc = min_discret
        val_discret = value * levels
        selected_value = max_discret

        d_values = [ min_discret + (step * i) for i in range(0, levels)]

        for d_v in d_values:
            if val_discret <= d_v:
                selected_value = d_v

        return selected_value

    def interval_discretization(self, vmin: float, vmax: float, value: float, levels: int) -> float:
        distance = vmax - vmin
        step = distance / levels

        min_discret = vmin
        d_values = [ round(min_discret + (step * i), 2) for i in range(0, levels) ]
        selected = min_discret

        for d_v in d_values:
            if value <= d_v:
                return d_v

        return selected

class QTable(controller_template.QTable):
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        self._table = dict()

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        gen_key = self.generate_key_for_state_action(key, action)
        return self._table.get(gen_key, 0)

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return: 
        """
        gen_key = self.generate_key_for_state_action(key, action)
        self._table[gen_key] = new_q_value

    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        qtable = QTable()
        
        with open(path, 'r') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            for row in csv_reader:
                value = float(row[-1:][0])
                key = str.join(',', row[:-1])
                qtable._table[key] = value        

        return qtable

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        with open(path, 'w+') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=',')
            for key, value in self._table.items():
                raw_row = key.split(',') + [value]
                csv_writer.writerow(raw_row)

    def generate_key_for_state_action(self, state : State, action : int) -> str:
        gen_key = ''
        descritized_values = state.get_discretized_features()
        for v in descritized_values:
            gen_key += str(v)+','
        gen_key += str(action)
        return gen_key

class Controller(controller_template.Controller):
    
    ACTION_LIST = [1, 2, 3, 4, 5]

    def __init__(self, q_table_path: str, alpha=0.5, gamma=0.99, T_MAX=10000, T_DECAY=0.1, reward_type='good_features'):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)
        self.T = T_MAX
        self.current_episode = -1
        self.higher = sys.float_info.min
        self.lower = sys.float_info.max
        self.checkpoint_count = 0
        self.alpha = alpha
        self.gamma = gamma
        self.T_MAX = T_MAX
        self.T_DECAY = T_DECAY
        self.reward_type = reward_type

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get to new_state
        :param reward: the reward the car received for getting to new_state  
        :param end_of_race: boolean indicating if a race timeout was reached
        """
        new_action = self.take_action(new_state, 1)
        q_value = self.q_table.get_q_value(old_state, action)
        q_sl_al = self.q_table.get_q_value(new_state, new_action)
        q_s_a_k_plus_1 = (1.0 - self.alpha) * q_value + self.alpha * (reward + self.gamma * q_sl_al)
        self.q_table.set_q_value(old_state, action, q_s_a_k_plus_1)

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_race: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the car just entered
        :param old_state: The state the car just left
        :param action: the action the car performed to get in new_state
        :param n_steps: number of steps the car has taken so far in the current race
        :param end_of_race: boolean indicating if a race timeout was reached
        :return: The reward to be given to the agent
        """
        reward_function = getattr(self, self.reward_type)
        return reward_function(new_state, old_state, action, n_steps, end_of_race)

    def few_features(self, new_state: State, old_state: State, action: int, n_steps: int, end_of_race: bool) -> float:
        old_discretized_features = old_state.get_discretized_features()
        old_inside_street_margin = old_discretized_features[0]
        old_dist_left = old_discretized_features[1]
        old_dist_right = old_discretized_features[2]
        old_dist_middle = old_discretized_features[3]
        old_checkpoint_distance = old_discretized_features[4]
        old_checkpoint = old_discretized_features[5]
        old_car_velocity = old_discretized_features[6]
        old_dist_detected_enemy = old_discretized_features[7]
        old_incoming_track = old_discretized_features[8]
        old_dist_detected_bomb = old_discretized_features[9]
        old_enemy_abs_angle = old_discretized_features[10]
        old_bomb_abs_angle = old_discretized_features[11]
        
        new_discretized_features = new_state.get_discretized_features()
        new_inside_street_margin = new_discretized_features[0]
        new_dist_left = new_discretized_features[1]
        new_dist_right = new_discretized_features[2]
        new_dist_middle = new_discretized_features[3]
        new_checkpoint_distance = new_discretized_features[4]
        new_checkpoint = new_discretized_features[5]
        new_car_velocity = new_discretized_features[6]
        new_dist_detected_enemy = new_discretized_features[7]
        new_incoming_track = new_discretized_features[8]
        new_dist_detected_bomb = new_discretized_features[9]
        new_enemy_abs_angle = new_discretized_features[10]
        new_bomb_abs_angle = new_discretized_features[11]

        reward = 0.0
        
        # count checkpoints passed throught
        self.checkpoint_count += new_checkpoint

        # closer to checkpoint
        direction_weighted_next_checkpoint = 10 - (new_checkpoint_distance - old_checkpoint_distance) * -1

        # check for acceleration increase or decrease for reward increment or decrement
        direction_car_velocity = (new_car_velocity - old_car_velocity) if new_state.on_track == TrackType.ON_TRACK else 0

        reward += new_inside_street_margin              # -1 if out of the track
        reward += new_dist_left                         # from -1 of out of the track to 1 in the right of the track
        reward += new_dist_right                        # from -1 of out of the track to 1 in the left of the track
        reward += 1.0 / math.exp(abs(new_dist_middle))  # receive 1 if closer to the middle 0 if farther from it
        reward += direction_weighted_next_checkpoint    # NOT checking the distance, computing the approximation to next checkpoint
        reward += self.checkpoint_count                 # sum total of checkpoints passed
        reward += direction_car_velocity                # reward acceleration on track, but not on ice or grass
        return reward

    def good_features(self, new_state: State, old_state: State, action: int, n_steps: int, end_of_race: bool) -> float:
        old_discretized_features = old_state.get_discretized_features()
        old_inside_street_margin = old_discretized_features[0]
        old_dist_left = old_discretized_features[1]
        old_dist_right = old_discretized_features[2]
        old_dist_middle = old_discretized_features[3]
        old_checkpoint_distance = old_discretized_features[4]
        old_checkpoint = old_discretized_features[5]
        old_car_velocity = old_discretized_features[6]
        old_dist_detected_enemy = old_discretized_features[7]
        old_incoming_track = old_discretized_features[8]
        old_dist_detected_bomb = old_discretized_features[9]
        old_enemy_abs_angle = old_discretized_features[10]
        old_bomb_abs_angle = old_discretized_features[11]
        
        new_discretized_features = new_state.get_discretized_features()
        new_inside_street_margin = new_discretized_features[0]
        new_dist_left = new_discretized_features[1]
        new_dist_right = new_discretized_features[2]
        new_dist_middle = new_discretized_features[3]
        new_checkpoint_distance = new_discretized_features[4]
        new_checkpoint = new_discretized_features[5]
        new_car_velocity = new_discretized_features[6]
        new_dist_detected_enemy = new_discretized_features[7]
        new_incoming_track = new_discretized_features[8]
        new_dist_detected_bomb = new_discretized_features[9]
        new_enemy_abs_angle = new_discretized_features[10]
        new_bomb_abs_angle = new_discretized_features[11]

        reward = 0.0
        
        # count checkpoints passed throught
        self.checkpoint_count += new_checkpoint

        # closer to checkpoint
        direction_weighted_next_checkpoint = 10 - (new_checkpoint_distance - old_checkpoint_distance) * -1

        # check for acceleration increase or decrease for reward increment or decrement
        direction_car_velocity = (new_car_velocity - old_car_velocity) if new_state.on_track == TrackType.ON_TRACK else 0

        # enemy is detect in new and old states
        enemy_was_and_is_detected = old_dist_detected_enemy != 0 and new_dist_detected_enemy != 0

        # bomb is detected in new and old states
        bomb_was_and_is_detected = old_dist_detected_bomb != 0 and new_dist_detected_bomb != 0

        # distance to enemy if detected
        diff_distance_to_detected_enemy = (new_dist_detected_enemy - old_dist_detected_enemy) if enemy_was_and_is_detected else 0

        # distance to bomb if detected
        diff_distance_to_detected_bomb = (new_dist_detected_bomb - old_dist_detected_bomb) if bomb_was_and_is_detected else 0 

        # valorization of near 0 and closest to 45º
        near_45_deg_close_to_enemy = new_enemy_abs_angle/45.0 * (1 - math.tanh(diff_distance_to_detected_enemy))
        near_45_deg_close_to_bomb = new_bomb_abs_angle/45.0 * (1 - math.tanh(diff_distance_to_detected_bomb))

        reward += new_inside_street_margin              # -1 if out of the track
        reward += new_dist_left                         # from -1 of out of the track to 1 in the right of the track
        reward += new_dist_right                        # from -1 of out of the track to 1 in the left of the track
        reward += 1.0 / math.exp(abs(new_dist_middle))  # receive 1 if closer to the middle 0 if farther from it
        reward += direction_weighted_next_checkpoint    # NOT checking the distance, computing the approximation to next checkpoint
        reward += self.checkpoint_count                 # sum total of checkpoints passed
        reward += diff_distance_to_detected_enemy       # devalorize distance increase to enemy when its detected up front and valorize distance increase when it's behind
        reward += new_incoming_track                    # + points if keep on street or ice
        reward += near_45_deg_close_to_bomb             # when near, try to avoid it by rotating to some of the sides giving more reward (close to 1)
        return reward


    def all_features(self, new_state: State, old_state: State, action: int, n_steps: int, end_of_race: bool) -> float:
        old_discretized_features = old_state.get_discretized_features()
        old_inside_street_margin = old_discretized_features[0]
        old_dist_left = old_discretized_features[1]
        old_dist_right = old_discretized_features[2]
        old_dist_middle = old_discretized_features[3]
        old_checkpoint_distance = old_discretized_features[4]
        old_checkpoint = old_discretized_features[5]
        old_car_velocity = old_discretized_features[6]
        old_dist_detected_enemy = old_discretized_features[7]
        old_incoming_track = old_discretized_features[8]
        old_dist_detected_bomb = old_discretized_features[9]
        old_enemy_abs_angle = old_discretized_features[10]
        old_bomb_abs_angle = old_discretized_features[11]
        
        new_discretized_features = new_state.get_discretized_features()
        new_inside_street_margin = new_discretized_features[0]
        new_dist_left = new_discretized_features[1]
        new_dist_right = new_discretized_features[2]
        new_dist_middle = new_discretized_features[3]
        new_checkpoint_distance = new_discretized_features[4]
        new_checkpoint = new_discretized_features[5]
        new_car_velocity = new_discretized_features[6]
        new_dist_detected_enemy = new_discretized_features[7]
        new_incoming_track = new_discretized_features[8]
        new_dist_detected_bomb = new_discretized_features[9]
        new_enemy_abs_angle = new_discretized_features[10]
        new_bomb_abs_angle = new_discretized_features[11]

        reward = 0.0
        
        # count checkpoints passed throught
        self.checkpoint_count += new_checkpoint

        # closer to checkpoint
        direction_weighted_next_checkpoint = 10 - (new_checkpoint_distance - old_checkpoint_distance) * -1

        # check for acceleration increase or decrease for reward increment or decrement
        direction_car_velocity = (new_car_velocity - old_car_velocity) if new_state.on_track == TrackType.ON_TRACK else 0

        # enemy is detect in new and old states
        enemy_was_and_is_detected = old_dist_detected_enemy != 0 and new_dist_detected_enemy != 0

        # bomb is detected in new and old states
        bomb_was_and_is_detected = old_dist_detected_bomb != 0 and new_dist_detected_bomb != 0

        # distance to enemy if detected
        diff_distance_to_detected_enemy = (new_dist_detected_enemy - old_dist_detected_enemy) if enemy_was_and_is_detected else 0

        # distance to bomb if detected
        diff_distance_to_detected_bomb = (new_dist_detected_bomb - old_dist_detected_bomb) if bomb_was_and_is_detected else 0 

        # valorization of near 0 and closest to 45º
        near_45_deg_close_to_enemy = new_enemy_abs_angle/45.0 * (1 - math.tanh(diff_distance_to_detected_enemy))
        near_45_deg_close_to_bomb = new_bomb_abs_angle/45.0 * (1 - math.tanh(diff_distance_to_detected_bomb))

        reward += new_inside_street_margin              # -1 if out of the track
        reward += new_dist_left                         # from -1 of out of the track to 1 in the right of the track
        reward += new_dist_right                        # from -1 of out of the track to 1 in the left of the track
        reward += 1.0 / math.exp(abs(new_dist_middle))  # receive 1 if closer to the middle 0 if farther from it
        reward += direction_weighted_next_checkpoint    # NOT checking the distance, computing the approximation to next checkpoint
        reward += self.checkpoint_count                 # sum total of checkpoints passed
        reward += direction_car_velocity                # reward acceleration on track, but not on ice or grass
        reward += diff_distance_to_detected_enemy       # devalorize distance increase to enemy when its detected up front and valorize distance increase when it's behind
        reward += new_incoming_track                    # + points if keep on street or ice
        reward += diff_distance_to_detected_bomb        # devalorize distance increase to bomb when its detected up front and valorize distance increase when it's behind
        reward += near_45_deg_close_to_enemy            # when near, try to avoid it by rotating to some of the sides giving more reward (close to 1)
        reward += near_45_deg_close_to_bomb             # when near, try to avoid it by rotating to some of the sides giving more reward (close to 1)
        return reward

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the car must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the car 
        :param episode_number: current episode/race during the training period
        :return: The action the car chooses to execute
        """
        if self.current_episode != episode_number:
            self.checkpoint_count = 0
        q_values = [ self.q_table.get_q_value(new_state, i) for i in Controller.ACTION_LIST ]

        discret_features = new_state.discretize_features(new_state.compute_features())

        if new_state.checkpoint_distance > self.higher and new_state.on_track >= TrackType.ON_TRACK:
            self.higher = new_state.checkpoint_distance

        if new_state.checkpoint_distance < self.lower and new_state.on_track >= TrackType.ON_TRACK:
            self.lower = new_state.checkpoint_distance
        
        try:
            # BOLTZMAN EXPLORATION
            selected = self.boltzman_exploration(q_values, episode_number)
        except:
            # HUNGRY APPROACH choose the best action from qtable
            selected = q_values.index(max(q_values)) + 1
            pass

        return selected

    def boltzman_exploration(self, q_values: list, episode_number: int) -> int:
        self.T = self.T - self.T * self.T_DECAY
        # reset T
        if self.current_episode != episode_number:
            self.T = self.T_MAX
            self.current_episode = episode_number

        denominator = numpy.sum([ math.exp(qv/self.T) for qv in q_values ])
        probability_distribution = [ math.exp(qv/self.T) / denominator for qv in q_values ]
        action = numpy.random.choice(Controller.ACTION_LIST, p=probability_distribution, replace=False)
        return action
        